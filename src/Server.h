#ifndef ASYNCHRONOUS_MULTITHREADED_SERVER_SERVER_H
#define ASYNCHRONOUS_MULTITHREADED_SERVER_SERVER_H

#include <boost/asio.hpp>
#include <string>
#include <vector>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include "connection.h"


class Server
        : private boost::noncopyable {
public:
    explicit Server(const std::string &address, const std::string &port, std::size_t thread_pool_size);

    void run();

private:
    void start_accept();

    void handle_accept(const boost::system::error_code &e);

    void handle_stop();

    std::size_t thread_pool_size_;
    boost::asio::io_context io_context_;
    boost::asio::signal_set signals_;
    boost::asio::ip::tcp::acceptor acceptor_;
    connection_ptr new_connection_;
};

#endif //ASYNCHRONOUS_MULTITHREADED_SERVER_SERVER_H
