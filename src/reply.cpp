#include "reply.h"

const std::string status_strings::ok = "HTTP/1.0 200 OK\r\n";
const std::string status_strings::internal_server_error = "HTTP/1.0 500 Internal Server Error\r\n";
const std::string status_strings::not_found = "HTTP/1.0 404 Not Found Error\r\n";

boost::asio::const_buffer status_strings::to_buffer(Reply::status_type status) {
    status_strings s;
    switch (status) {
        case Reply::ok:
            return boost::asio::buffer(status_strings::ok);
        case Reply::not_found:
            return boost::asio::buffer(status_strings::not_found);
        default:
            return boost::asio::buffer(status_strings::internal_server_error);
    }
}

std::vector<boost::asio::const_buffer> Reply::to_buffers() {
    std::vector<boost::asio::const_buffer> buffers;
    buffers.push_back(status_strings::to_buffer(status));
    buffers.emplace_back(boost::asio::buffer(headers));
    content = "\n" + content;
    buffers.emplace_back(boost::asio::buffer(content));
    return buffers;
}

const char stock_replies::ok[] = "";
const char stock_replies::internal_server_error[] =
        "<html><head><title>Internal Server Error</title></head><body><h1>500 Internal Server Error</h1></body></html>";
const char stock_replies::not_found[] =
        "<html><head><title>Not Found Error</title></head><body><h1>404 Not Found Error</h1></body></html>";


Reply Reply::stock_reply(Reply::status_type status) {
    Reply rep;
    rep.status = status;
    rep.content = stock_replies::to_string(status);
    rep.headers = "Content-Length: " +
            boost::lexical_cast<std::string>(rep.content.size()) + "\r\nContent-Type: text/html\r\n";
    return rep;
}