#include "Server.h"
#include <boost/thread/thread.hpp>


Server::Server(const std::string &address, const std::string &port, std::size_t thread_pool_size)
        : thread_pool_size_(thread_pool_size),
          signals_(io_context_),
          acceptor_(io_context_),
          new_connection_() {
    signals_.async_wait(boost::bind(&Server::handle_stop, this));
    boost::asio::ip::tcp::resolver resolver(io_context_);
    boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(address, port).begin();
    acceptor_.open(endpoint.protocol());
    acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    acceptor_.bind(endpoint);
    acceptor_.listen();

    start_accept();
}

void Server::run() {
    std::vector<boost::shared_ptr<boost::thread> > threads;
    for (std::size_t i = 0; i < thread_pool_size_; ++i) {
        boost::shared_ptr<boost::thread> thread(new boost::thread(
                boost::bind(&boost::asio::io_context::run, &io_context_)));
        threads.push_back(thread);
    }

    for (auto &thread : threads)
        thread->join();
}

void Server::start_accept() {
    new_connection_.reset(new Connection(io_context_));
    acceptor_.async_accept(new_connection_->socket(),
                           boost::bind(&Server::handle_accept, this,
                                       boost::asio::placeholders::error));
}

void Server::handle_accept(const boost::system::error_code &e) {
    if (!e) {
        new_connection_->start();
    }
    start_accept();
}

void Server::handle_stop() {
    io_context_.stop();
}
