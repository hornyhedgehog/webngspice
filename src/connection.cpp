#include "connection.h"
#include <regex>
#include <boost/bind.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/tuple/tuple.hpp>
#include <fstream>

Connection::Connection(boost::asio::io_context &io_context)
        : strand_(io_context),
          socket_(io_context){}

boost::asio::ip::tcp::socket &Connection::socket() {
    return socket_;
}

void Connection::start() {
    socket_.async_read_some(boost::asio::buffer(buffer_),
                            boost::asio::bind_executor(strand_,
                                                       boost::bind(&Connection::handle_read, shared_from_this(),
                                                                   boost::asio::placeholders::error,
                                                                   boost::asio::placeholders::bytes_transferred)));
}

bool consume(std::string &req_uri, const std::string &input) {
    std::regex rgx_for_html_page(".*GET /(.*) HTTP/.*");
    std::regex rgx_for_host(".*Host: (.*)");

    std::smatch match;
    std::smatch host_match;

    if (!std::regex_search(input.begin(), input.end(), match, rgx_for_html_page))
        return false;

    std::regex_search(input.begin(), input.end(), host_match, rgx_for_host);

    req_uri = "http://" + std::string{host_match[1]} + "/" + std::string{match[1]};
    return true;
}

boost::tuple<bool, char *> parse(std::string &req, char *begin, const char *end) {
    std::string to_parse{};
    while (begin != end) {
        to_parse += *begin++;
    }
    auto result = consume(req, to_parse);

    return boost::make_tuple(result, begin);
}

std::string get_requested_path(const std::string &req_uri){
    std::string request_path;
    request_path.clear();
    request_path.reserve(req_uri.size());
    auto i = req_uri.find_last_of('/');
    request_path = req_uri.substr(i + 1, req_uri.length());

    if(request_path.length() == 0){
        request_path = "index.html";
    }
    return "../sources/" + request_path;
}

int read_file_into_string(const std::string &path, std::string &result){
    std::ifstream t(path);
    if (!t.is_open()) {
        return -1;
    }
    std::stringstream buffer;
    buffer << t.rdbuf();
    result = buffer.str();
    return 0;
}

void handle_request(const std::string &req_uri, Reply &rep) {
    std::string content;
    if(read_file_into_string(get_requested_path(req_uri), content) == -1){
        rep = Reply::stock_reply(Reply::not_found);
        return;
    }
    rep.status = Reply::ok;
    rep.content = content;
    rep.headers = "Content-Length: " + boost::lexical_cast<std::string>(rep.content.size()) +
                  "\r\nContent-Type: text/html\r\n";
}

void Connection::handle_read(const boost::system::error_code &e,
                             std::size_t bytes_transferred) {
    if (!e) {
        boost::tribool result;
        boost::tie(result, boost::tuples::ignore) = parse(
                request_, buffer_.data(), buffer_.data() + bytes_transferred);

        if (result) {
            handle_request(request_, reply_);
            boost::asio::async_write(socket_, reply_.to_buffers(),
                                     boost::asio::bind_executor(strand_,
                                                                boost::bind(&Connection::handle_write,
                                                                            shared_from_this(),
                                                                            boost::asio::placeholders::error)));
        } else if (!result) {
            reply_ = Reply::stock_reply(Reply::internal_server_error);
            boost::asio::async_write(socket_, reply_.to_buffers(),
                                     boost::asio::bind_executor(strand_,
                                                                boost::bind(&Connection::handle_write,
                                                                            shared_from_this(),
                                                                            boost::asio::placeholders::error)));
        } else {
            socket_.async_read_some(boost::asio::buffer(buffer_),
                                    boost::asio::bind_executor(strand_,
                                                               boost::bind(&Connection::handle_read, shared_from_this(),
                                                                           boost::asio::placeholders::error,
                                                                           boost::asio::placeholders::bytes_transferred)));
        }
    }
}

void Connection::handle_write(const boost::system::error_code &e) {
    if (!e) {
        boost::system::error_code ignored_ec;
        socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ignored_ec);
    }
}